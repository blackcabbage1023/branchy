#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  8 17:16:08 2018

@author: joaosilva
"""

def initialize(context):
    context.security = symbol('TSLA')
    schedule_function(func=action_function, 
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_open(hours=0,minutes=1)
                      )

def action_function(context, data):
    price_history = data.history(context.security,
                                 fields = 'price',
                                 bar_count = 50,
                                 frequency = '5d',
                                 )
    average_price = price_history.mean()
    current_price = data.current(assets = context.security,
                                 fields = 'price'
                                 )
    if data.can_trade(context.security):
        if current_price > average_price * 1.03:
            order_target_percent(context.security, 1)
        else:
            order_target_percent(context.security, 0)   
                      
print('hello world')
print('another branch')
